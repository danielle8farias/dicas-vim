# Inserindo texto e escrevendo no VIM / NeoVIM

Para começar a escrever no VIM, pressione a **tecla i** e o modo de inserção de texto será iniciado no local onde está o cursor.

```
i
```

Pressionando a **tecla I** (i maiúsculo) o modo inserção inicia no começo da linha onde está a localização atual do cursor.

```
I
```

Pressionando a **tecla a** o modo inserção inicia um caractere a frente da localização atual do cursor.

```
a
```

Pressionando a **tecla A** o modo inserção inicia no final da linha onde está a localização atual do cursor.

```
A
```

Pressionando a **tecla o** (o minúsculo) o modo inserção inicia na linha seguinte de onde está a localização atual do cursor.

```
o
```

Pressionando a **tecla O**  (o maiúsculo) o modo inserção inicia na linha acima de onde está a localização atual do cursor.

```
O
```

Para voltar aos comando do VIM, basta pressionar o **Esc**.

tags: vim, neovim, insert,
