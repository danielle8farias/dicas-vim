# Abrindo o VIM / NeoVIM

Para abrir o VIM ou NeoVIM, basta digitar no terminal:

```
$ vim
```

- **$** indica que você deve usar o usuário comum para fazer essa operação.

O que aparecerá é algo semelhante a imagem abaixo.

![abrindo vim sem nome](img/p0001-0.png)

Também é possível abrir o VIM ao mesmo tempo em que se cria um arquivo.

```
$ vim <novo arquivo>
```

- digite o nome do arquivo que deseja criar sem os sinais **< e >**.

Exemplo:

![abrindo e criando documento](img/p0001-1.png)

![novo arquivo no vim](img/p0001-2.png)

Apesar do arquivo ter sido criado, ele ainda não foi salvo. Ou seja, ele existe nos arquivos temporários e se você fechar o editor provavelmente não terá mais acesso a ele.

Outro modo é abrindo um arquivo já existente:

```
$ vim <arquivo>
```

Exemplo:

![abrindo arquivo existente com o vim](img/p0001-3.png)

![arquivo aberto no vim](img/p0001-4.png)

Marcado em vermelho, na imagem, temos o nome do arquivo e sua extensão, o número de linhas e o número de colunas do texto.

tags: vim, neovim, editor
