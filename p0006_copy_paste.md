# Copiar, cortar e colar no VIM / NeoVim


Para copiar o texto da linha onde está o cursor

```
yy
```

Para recortar o texto da linha onde está o cursor

```
dd
```

Para colar o texto copiado no local onde está o cursor pressione a **tecla p** (p minúsculo)

```
p
```

Caso queira copiar a palavra onde está o cursor, digite

```
yw
```

Para selecionar um trecho do texto com o curso, é preciso entrar no modo visual pressionando a **tecla v** (v minúsculo)

```
v
```

Esse comando seleciona as linhas de um texto.

![modo visual do vim](img/p0006-0.gif)

Com o texto selecionado, pressione a **tecla y** (y minúsculo) para copiar

```
y
```

Ou a **tecla d** (d minúsculo), caso queira recortar o trecho selecionado

```
d
```

Caso queira selecionar as colunas, pressione **CTRL+v** (tecla Control e tecla v).

Para fazer uma cópia do arquivo aberto, para um novo arquivo, digite

```
:w <novo nome do arquivo>
```

- digite o novo nome para o arquivo sem os sinais **< e >**.

![copiando todo conteúdo do arquivo para um novo](img/p0006-1.gif)

tags: vim, neovim, copiar, colar, recortar
