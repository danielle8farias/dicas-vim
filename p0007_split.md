# Trabalhando com mais de um arquivo ao mesmo tempo no VIM / NeoVIM

## Vários arquivos na mesma sessão do VIM

Caso queira abrir vários arquivos na mesmo sessão do VIM, digite

```
$ vim -o <nome do primeiro arquivo> <nome do segundo arquivo>
```

- **$** indica que você deve usar o usuário comum para fazer essa operação.
- **o**, do inglês *open*, para abrir vários arquivos na mesma sessão.
- digite os nomes dos arquivos sem os sinais **< e >**.

## Arquivos existentes e um novo na mesma sessão do VIM

Caso deseja abrir um arquivo em branco junto a dois arquivos já existentes, basta indicar ao VIM o número de arquivos.

Exemplo:

```
$ vim -o3 index.html estilo.css
```

Vou abrir dois arquivos já existentes (index.html e estilo.css) e um arquivo novo em branco.

![abrindo 3 arquivos no vim](img/p0007-0.png)

## Abrindo novo arquivo na sessão do VIM

Caso já esteja com uma sessão aberta e pretenda abrir um novo arquivo,

```
:split <nome do outro arquivo>
```

![abrindo segundo arquivo junto ao primeiro](img/p0007-1.gif)

## Alternando entre as telas do VIM

Pressione **CRTL+ww** (tecla Control e tecla w duas vezes) para alternar entre as telas.

![alternando entre os textos](img/p0007-2.png)

![alternando entre os textos](img/p0007-3.png)

Perceba que o **tela ativa** fica com o nome em negrito, como mostrado nas imagens acima.

## Abrindo arquivos em tela vertical

Caso queira que a divisão da tela seja na vertical, digite

```
:vsplit <nome do outro arquivo>
```

## Dividindo a tela do mesmo arquivo

Com uma sessão do VIM aberta e digitando apenas

```
:split
```

Sem mencionar qualquer nome de arquivo, o VIM vai dividir o mesmo arquivo da sessão atual em duas telas. Isso é útil para quando se está trabalhando em arquivos muito grandes e é necessário verificar partes diferentes do código ao mesmo tempo.

Atenção: esse comando cria um clone da sessão atual que é atualizado em tempo real.

## Abrindo arquivos em branco na sessão do VIM

Sempre é possível abrir novas janelas, com arquivos em branco, na mesmo sessão com os comandos

```
:new
```

e

```
:vnew
```

para telas verticais.

## Saindo da sessão do VIM com várias telas

Para sair de todos os arquivos abertos numa mesma sessão, digite

```
:qa
```

ou

```
:qa!
```

Para sair sem salvar as alterações feitas nos arquivos.

tags: vim, neovim, split
