# Saindo do VIM / NeoVIM

Para sair do VIM de um arquivo já salvo, digite 

```
:q
```

Para sair do arquivo modificado sem salvar as alterações, digite o comando

```
:q!
```

tags: vim, neovim, exit, sair
