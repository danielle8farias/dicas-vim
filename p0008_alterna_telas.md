## Navegando entre as várias telas da mesma sessão do VIM

Pressionando **CRTL + ww** (tecla Control e tecla w duas vezes), o VIM alterna entre as telas abertas na sessão. Esse comando move o cursor para a tela seguinte e é cíclico, ou seja, ao chegar a última tela, você pode com ele retornar a primeira tela.

Pressionando **CRTL + w + j** ou **CRTL + w + seta para baixo**, o VIM alterna para a próxima tela abaixo.

Pressionando **CRTL + w + k** ou **CRTL + w + seta para cima**, o VIM alterna para a próxima tela acima.

Pressionando **CRTL + w + h** ou **CRTL + w + seta para esquerda**, o VIM alterna para a tela à esquerda.

Pressionando **CRTL + w + L** ou **CRTL + w + seta para direita**, o VIM alterna para a tela à direita.

Perceba que o **tela ativa** fica com o nome em negrito, como mostrado nas imagens acima.

tags: vim, neovim, telas
