# Instalando o VIM/NeoVIM

Caso você ainda não tenha o VIM instalado na sua máquina, digite:

```
$ sudo apt install vim
```

- **$** indica que você deve usar o usuário comum para fazer essa operação.
- **sudo** serve para pedir permissões de administrador temporariamente.
- **apt** do inglês, *Advanced Package Tool*, em português, *Ferramenta de Empacotamento Avançada*; é a ferramenta que nos ajuda na instalação, atualização e desinstalação de programas, entre outras funções.
- **install** é o comando de instalar, indicando ao apt o que fazer.

ou

```
$ sudo apt install neovim
```

para instalar o [NeoVIM](https://neovim.io/charter/).

tags: vim, neovim, editor
