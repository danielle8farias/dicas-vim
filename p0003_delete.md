# Excluindo linha ou caracteres no VIM / NeoVIM

Pressionando a **tecla S** (S maiúsculo) toda linha onde está o cursor é apagada e em seguida o VIM entrará no modo inserção.

```
S
```

Também é possível excluir a linha onde está o cursor sem entrar no modo de inserção, basta digitar

```
dd
```

A **tecla X** (X maiúsculo) funciona como a tecla *Backspace* 

```
X
```

E a **tecla x** (x minúsculo) funciona como o *Delete*.

```
x
```

tags: neovim, vim, x, delete
