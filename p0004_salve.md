# Salvando um arquivo no VIM / NeoVIM

Para salvar o arquivo no qual você está trabalhando atualmente, digite

```
:w
```

Digitando 

```
:wq
```
ou 

```
:x 
```

ou ainda pressionando **SHIFT** e **ZZ** juntos, o VIM salva e fecha o arquivo atual.

tags: vim, neovim, salve
